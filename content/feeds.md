---
title: NorfolkNest Blog RSS Feeds
date: 2010-03-14T20:51:28.000+13:00
type: feeds

---
These are RSS/XML formatted feeds for accessing posts and podcasts of the NorfolkNest Blog, they can be used by copying the links location and adding it to your feed reader of choice such as [feedly.com](feedly.com) or [feeder.co](feeder.co)

List of all posts on the blog

[Post.xml](https://jaybigguy10.gitlab.io/post/index.xml)

List of all posts currently on the front page of the blog

[Index.xml](https://jaybigguy10.gitlab.io/index.xml)

A sitemap of the blog

[Sitemap.xml](https://jaybigguy10.gitlab.io/sitemap.xml)

The podcast links

[Podcast.rss](http://jaybigguy10.gitlab.io/norfolknetblog/podcast.rss)

[Podcast.xml](http://jaybigguy10.gitlab.io/norfolknetblog/podcast.xml)