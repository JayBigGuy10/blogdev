+++
date = 2020-06-09T12:00:00Z
title = "Paying the (Cheap) first gen hardware price - Galaxy Buds SM-R170 in 2020"

+++
I recently brought a pair of the first generation galaxy buds in mid 2020, here is what my thoughts are about them from someone who had only ever worn wired apple earbuds.

## Why did I buy now? - Cost

The galaxy buds released on the 8th of March 2019 at an initial price of NZD $289.00 and has been steadily trending down. I technically brought them when they were $167 but they came out at a total of $180 including credit card surcharge and shipping. The larger slightly more expensive companies here ran out of stock and after phoning them up they said they had no plans on getting them back in stock, I didn't want to risk not being able to buy them brand new so I pulled the trigger on them. Riding the line between the cheapest price and not being stocked brand new isn't my skill though as they have now dropped to $149.99 on the cheap sites and come back in stock on the slightly costlier ones.

![](http://jaybigguy10.gitlab.io/blogdev/img/pricegraph.PNG)

## Interesting Features

These buds are the first USB-C connected device that I have owned, I have no problems with how the connector works but I just have to make sure that I have another cable with me.

## The price of first gen - Quirks & Complaints

Keep in mind that on higher end phones these issues will probably not be an issue but I am currently running a Huawei nova 3i. The major issue that I am experiencing is hard to pin down the exact fault to but I listen to lots of podcasts with the pocketcasts app and occasionally when I use either the play button in the app or on the galaxy bud the podcast will start playing but no sound comes out of the galaxy buds, to fix this I have to play/pause the podcast several times and occasionally have to reconnect them which leads to me needing to slightly rewind the podcast. Another unique issue that I have had that hasn't occurred as often and has gotten better is when opening the case and putting the buds in without turning on my phone the buds will connect in calls only mode and refuse to play any music or other media until a reconnect is done.

The major quirk which is probably not seen by anyone with higher end devices is that if you configure the hold action to control volume on some devices it doesn't change the on device volume. The effect of this is that the device can be at max volume and the audio still come out quietly on the buds, I have only observed this on my android device as on windows it seems to behave normally.

In the app there is a feature to play a loud alert sound to locate the buds if they become lost, this alert sound is limited to bird chirps, which needs to be changed or have other loud noise options as if you **hypothetically** happen to drop one in grass under a tree it can become very hard to tell whether you can hear the galaxy bud or a bird in the tree above.

## November 2020 Update

I now own a Oneplus Nord which I now use the galaxy buds with which has Bluetooth v5.1 compared to my nova 3i's v4.2 Bluetooth. This has significantly improved having the buds connect properly and without having to check if they are connected before starting media playback. I dont have any situations where audio just doesnt come out but I have had to repair them once to make the touch controlls work again.

The new phone also allows me to watch 1080p 2x Youtube because it has 5Ghz wifi, so beware on any cheaper or older phone with only 2.4Ghz wifi it has to share the frequency/bandwidth with bluetooth which can cause some buffering