---
date: 2020-03-26T02:00:00Z
draft: true
title: "How to: CMS with git pages and a static site generator"
---
I host my blog with gitlab pages as shown in this guide (link here) for free wich is a huge benefit for a college student like me but this means my blog doesn't have a cms.

A cms is a content management system or user interface for your blog, it allows for much easier creation of content and adding that content to the blog as it gives a rich text editor experience which is connected to the sites server, an example of the most well known cms is WordPress and a list of others can be found [here from Wikipedia](https://en.m.wikipedia.org/wiki/List_of_content_management_systems?wprov=sfla1)

Wordpress will not work with a blog hosted on gitlab/hub pages as it is more of an all in one product, it also means we cant track it with git if you want to as wordpress posts are stored in a database file, this is why for this I am using [forestry.io](https://forestry.io) for this as it doesnt handle nearly as much of the process as its basically a markdown editor hooked up to commit to a git repository of your choosing. The markdown files are then plugged into the static site generator which can be hosted for free on gitlab pages which then renders them. Basically forestry editor - git commit to repo - static site generation - git pages, here is how you do it. 

First convert your plain html website into a static site generation tool template, (here is a fast guide on how to do this link here)