---
title: "How I Built A Deck With Built In Home Assistant Controlled Lighting"
date: 2019-10-02T11:06:35+13:00
draft: false
---

As part of the deck that we built in the house that I live in I wanted to have built in LED Strips that I could control from Home Assistant, the deck is in between the house and some retaining walls so I planned on putting a strip of LED's between each post off the wall, a strip underneath the breakfast bar and a strip on each step. 
My plan for controlling the leds was to run 240v ac cables to near the start of each strip of leds and switch the high voltage with a 4 channel sonoff wifi switch
I bought the Led Strips from AliExpress and most of the other supplies came from the local hardware store

I bought 3 of these [Led Strips](https://www.aliexpress.com/item/32711632041.html?spm=a2g0s.9042311.0.0.27424c4d9huLMC) at 5 Meter lengths, as it was cheaper than buying 1 meter strips, in white with IP67 water resistance.
![Deckimage18](../../img/2019/deck-18.jpg)

The [Eletrical Conduit Tubes](https://www.mitre10.co.nz/shop/marley-rigid-conduit-pipe-medium-duty-4m-25mm-grey/p/103203) and [Conduit Corners](https://www.mitre10.co.nz/shop/marley-conduit-solid-elbow-25mm-grey/p/103196) along with other mounting hardware to run the 240V cables underneath the deck were just bought from the local Mitre10 in roughly estimated lengths.
![Deckimage1](../../img/2019/deck-1.jpg)

To connect the Leds up we used lots of [speaker wire](https://www.jaycar.co.nz/medium-duty-fig-8-speaker-cable-sold-per-metre/p/WB1706) (I belive this is the right product) and [terminal strips](https://www.jaycar.co.nz/10-amp-12-way-screw-terminal-strip/p/HM3196)
![Deckimage19](../../img/2019/deck-19.jpg)

I bought two of these [Small Power Supplies](http://ledbulbs.co.nz/led-powersupplies/30W-12v-led-powersupply) because the led strips pull 3.84 Watts/m meaning 30 Watts is sufficent for the stairs and under the breakfast bar and a [Large Power Supply](http://ledbulbs.co.nz/led-powersupplies/60W-Led-powersupply) to power the long strip on the retaining wall, 
To switch the wall power I bought a [Sonoff 4ch R2](https://www.aliexpress.com/item/32916753153.html?spm=a2g0o.productlist.0.0.75fc198asoo8lr&algo_pvid=7ac85133-d0c4-4684-98c0-15b540d21212&algo_expid=7ac85133-d0c4-4684-98c0-15b540d21212-1&btsid=e0fa9216-715e-4798-abc7-934aca6f29da&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_52) which in hindsight I could have got an esp8266 and a relay board for much cheaper but this was much easier to install as it is all in one package, 
I didn't want to use the default firmware that the sonoff comes with so I bought a [USB to TTL adaptor](https://www.aliexpress.com/item/32634246159.html?spm=a2g0s.9042311.0.0.17eb4c4dbprOxR) to flash it with [these instructions](https://www.youtube.com/watch?v=VZVJTD6tq2A) which after powering it up with wall power was still showing the factory firmware hotspot so i did a factory reset by holding down channel 4 for 40 seconds and from there it was a normal tasmota setup


First we ran a conduit from below the outdoor powerpoint already on the exterior of the house into a t-junction on the retaining wall.
![Deckimage](../../img/2019/deck-2.jpg)
![Deckimage](../../img/2019/deck-4.jpg)

From the t-junction we ran conduit to both ends of the retaining wall.
![Deckimage](../../img/2019/deck-5.jpg)
![Deckimage](../../img/2019/deck-6.jpg)

We then ran a second conduit from the outdoor powerpoint to where the breakfast bar is.
![Deckimage](../../img/2019/deck-8.jpg)
![Deckimage](../../img/2019/deck-9.jpg)
![Deckimage](../../img/2019/deck-10.jpg)
![Deckimage](../../img/2019/deck-12.jpg)

We pulled all four of the cables up into an enclosure with the Sonoff switch in it
![Deckimage](../../img/2019/deck-3.jpg)

The Aluminium profile was mounted to the underside of the bar and retaining wall with the clips that came with them, on the retaining wall at each end we drilled holes to run the wires through.
![Deckimage](../../img/2019/deck-14.jpg)
![Deckimage](../../img/2019/deck-15.jpg)
![Deckimage](../../img/2019/deck-16.jpg)

On the stairs the strips were mounted by just putting screws through the back of the profile into the steps
![Deckimage](../../img/2019/deck-17.jpg)

As I bought the led strips in three long rolls of five meters each I had to cut up the strips to the right lengths to fit in between the bays and on the stairs which was a challenge as the strips of leds that I bought were divided up into about 5 cm sections that can only be cut at certain points and solder on wires to the pads to be able to connect by scraping back the rear of the lights, I tried to keep the connections watertight by putting heatshrink over them
![Deckimage](../../img/2019/deck-20.jpg)
![Deckimage](../../img/2019/deck-21.jpg)

Test fitting and running the leds before clipping the profile back on
![Deckimage](../../img/2019/deck-22.jpg)

Leds in place being powered by a old 12v landline powersupply which was grossly underrated if we were to run the whole strip off of it
![Deckimage](../../img/2019/deck-23.jpg)

The power supplies for the Wall and Stairs all wired in
![Deckimage](../../img/2019/deck-25.jpg)

Uh Oh.. I hit a majour snag here by making a stupid mistake, can you see it?
![Deckimage](../../img/2019/deck-27.jpg)

As I was putting boards on the bar I put screws through the conduit and cable from both sides connecting live and ground with one and ground with neutral, this was only discovered when using a multimeter to test continuity between the cores to see which wire ran where, luckily the copper wasn't broken and it was wrapped up with eletrical tape.
![Deckimage](../../img/2019/deck-28.jpg)

The aftermath, due to having to take the bar off and breaking several screw heads in the process
![Deckimage](../../img/2019/deck-29.jpg)

The powersupply for the bar properly mounted without putting screws through the conduit
![Deckimage](../../img/2019/deck-30.jpg)

Wiring up mains into the sonoff enclosure, we took power from the outdoor switch, ran it back up the wall to a switch and back down into the enclosure
![Deckimage](../../img/2019/deck-31.jpg)

We added a manual override with a extra light switch we had left over incase we needed to reboot the sonoff or cut the power for safety without having to kill everything else on the breakers circuit
![Deckimage](../../img/2019/deck-32.jpg)

The final wiring of the sonoff before the lid was put on
![Deckimage](../../img/2019/deck-33.jpg)

Here are the photos of the glorious completed product
![Deckimage](../../img/2019/deck-24.jpg)
![Deckimage](../../img/2019/deck-34.jpg)
![Deckimage](../../img/2019/deck-35.jpg)
![Deckimage](../../img/2019/deck-36.jpg)

Here is how they are configured in home Assistant
![Deckimage](../../img/2019/deck-37.PNG)

<script src="https://gitlab.com/snippets/1900101.js"></script>
<script src="https://gitlab.com/snippets/1900102.js"></script>
				