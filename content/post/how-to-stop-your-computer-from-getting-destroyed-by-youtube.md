+++
date = 2020-05-14T12:00:00Z
title = "How to stop your computer from getting destroyed by YouTube"

+++
## Situation

I usually like to consume my YouTube videos at the ADHD inducing rates of 1.75x to 2x speed, however I have noticed that when I do this my laptop starts to make lots of airplane like noise during playback. At first I thought it was just my aging CPU/GPU in my laptop and opening task manager it revealed that the CPU usage sits around 50% to 60% and occasionally spiking to 80% when playing at 2x.

## The Diagnosis

But a i7-6500U and a GTX 950M shouldn't be chugging on YouTube this hard yet in my opinion, especially since my budget smartphone can do it no sweat, so I went looking for anyone else having this issue. It turns out that fixing this issue is pretty easy, the issue itself stems from the VP8/9 video encoding used by YouTube which my machine didn't have any native hardware decoding capability for and so shifts the load of video decode off the efficient dedicated hardware onto the slow in comparison CPU causing the high usage.

## Current Solution

So comes to the rescue the extension H264ify which:

> Makes YouTube stream H.264 videos instead of VP8/VP9 videos.
>
> Try h264ify if YouTube videos stutter, take up too much CPU, eat battery life, or make your laptop hot. ([Chrome Web Store](https://chrome.google.com/webstore/detail/h264ify/aleakchihdccplidncghkekgioiakgal)) ([GitHub](https://github.com/erkserkserks/h264ify))
>
> ![](http://jaybigguy10.gitlab.io/blogdev/img/h264.png)

this shifts the load back into the dedicated GPU video decode by forcing YouTube to use the more widely supported H.264 video format. If you are so inclined I would recommend you install the enhanced-H264ify which allows you to:

> Choose what video codec YouTube should play for you.
>
> This extension has new features such as selective blocking of H264, VP8, VP9, AV1 codecs and 60fps video. ([Chrome Web Store](https://chrome.google.com/webstore/detail/enhanced-h264ify/omkfmpieigblcllmkgbflkikinpkodlk)) ([GitHub](https://github.com/alextrv/enhanced-h264ify))
>
> ![](http://jaybigguy10.gitlab.io/blogdev/img/eh264.png)

## Further Thoughts & Conclusion

I think that this issue is one that YouTube should fix themselves as this shouldn't be up to a 3rd party extension to do. The best solution would be for them do some kind of automatic detection of hardware and use the best codec for the situation so that most users don't have to think about it, but even though I am not familiar with most digital privacy laws I think it might be of concern if YouTube was able to figure out the exact hardware of a user, a better solution might be to just put a per device option somewhere in the settings and create some documentation around what it fixes and how to use it.