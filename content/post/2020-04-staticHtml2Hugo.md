---
title: How to convert static html websites to a site theme
date: 2020-03-31T21:08:49.000Z
draft: true
---
So you have your beautiful website that you have made in plain html, it looks great and maybe has one or two posts on it. You try to create more content for it but the workflow is very hard to work with, write post as plain html, edit plain html of several other web pages to reference the post such as your index and archive, then upload it to your host. This process of content creation is even worse should someone unfamiliar with programming try to write a post for your website, it also locks you into your website theme forever unless you are willing to change the raw html for each and every post you have ever made.

The solution to this is static site generation, a tool like [Hugo](https://gohugo.io/ "Hugo") allows you to write your posts in a comfortable markdown format and the theme for the site can be changed by modifying a couple of template files which will reflect across the whole site.

If you are developing your theme it is a good idea to have a local install, follow [this guide](https://gohugo.io/getting-started/installing/#windows "guide") to install Hugo, the instructions do vary from platform to platform. The generation of the static files can be done two seperate ways:

* Local Generation - This is done by running just Hugo in the site folder and it will create a /public with all the html rendered files in it to upload to a hosting service
* Gitlab Pages Repository - By creating a new repository in gitlab from the Hugo template, running a ci/cd pipeline and then pushing your site to it, the files will be automatically generated and hosted. This allows you to use a CMS service like forestry.io or netlify CMS.



then from the command line navigate to the folder you want to store your site in and run <br>**hugo new site \[sitename]**