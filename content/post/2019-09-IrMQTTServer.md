---
title: "How to make A IR MQTT Server with A Wemos D1 Mini"
date: 2019-09-20T18:41:09+13:00
draft: false
---

I wanted a way to control my cinema amplifier, house sound system and dvd player and i didn't want to spend $50 on a 
[broadlink rm mini](https://www.amazon.com/dp/B07K2DHXB6/ref=cm_sw_r_tw_dp_U_x_3kiBEbY0ED7J3)

I already have an IR system installed in my house with three seperate recievers in the walls, one in the garage, one in the living room and one in the media rooom
which are connected with cat6 cable (just soldered on to three of the cores) to a hub block behind the stack of amps and dvd players
this hub then uses ir emmitter dots to relay the message to the stack.

How to:  
Note: this guide assumes you know how to use the arduino ide, esp8266, maybe solder, have home assistant, mqtt and how ir codes work 
 
I got a [Wemos D1 mini](https://www.aliexpress.com/item/32958249491.html?spm=a2g0s.9042311.0.0.27424c4dvjYTJi),
[ir leds](https://www.aliexpress.com/item/32793274994.html?spm=a2g0s.9042311.0.0.27424c4dvjYTJi) and 
[ir recivers](https://www.aliexpress.com/item/32491780864.html?spm=a2g0s.9042311.0.0.27424c4dUcLXPR) that I ordered from aliexpress and
the connecter block that I have is similar to this one on <a href='https://www.amazon.com/C2G-40430-Infrared-Repeater-Compliant/dp/B001BLTDZA'>amazon</a>,
while this one isn't cheaper than a broadlink rm if you can find one similar to this block it should work  

The arduino librarys that you will need are:   
PubSubClient by Nick O'Leary    
ArduinoJson by Benoit Blanchon  
IRremote by shirriff    
IRremoteESP8266 by David Conran     
WifiManager by tzapu

change line 26 to #define MQTT_MAX_PACKET_SIZE 1024 in pubsubclient.h which can be found at Documents\Arduino\libraries\PubSubClient\src then
once these are installed navigate to file>examples>IRremoteESP8266>IRMQTTServer. 
Flash this to the esp8266 and power it up

Connect to the wifi network name that starts with esp and put your wifi networks credentials and mqtt server info, then connect pins D2(gpio4) and Ground on the Wemos D1 Mini to Ground and signal on the block

![irImage](../../img/2019/post1-1.jpg)
![irImage](../../img/2019/post1-2.jpg)
![irImage](../../img/2019/post1-3.jpg)
![irImage](../../img/2019/post1-4.jpg)

power the wemos back up and check its web based interface from its ip that will have to be found from the router, 
this is an example config for home assistant which will change the volume on a yamaha amplifier 
 
<script src="https://gitlab.com/snippets/1952841.js"></script>

