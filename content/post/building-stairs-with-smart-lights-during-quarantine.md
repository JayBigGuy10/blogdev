---
date: 2020-05-29T12:00:00.000+00:00
title: Building stairs with smart lights during quarantine

---
## The Situation

In the before time when quarantine hadn't yet happened here in New Zealand we were planning on building stairs to connect the driveway to the deck with its home assistant controlled LEDs ([Project Post](https://jaybigguy10.gitlab.io/post/2019-10-decklights/ "Project Post")) , we managed to pick up some of the supplies to build the stairs the day before the announced quarantine went into effect. Here is a rough idea of what we were hoping to achieve, with the deck starting in the upper right and the platform that joins to the driveway on the lower left there would be a 1m strip of LEDs on the front face of each step (eleven in total).

![](http://jaybigguy10.gitlab.io/blogdev/img/Scan.png)Here is the worksite, the holes had been dug with string lines between them for a couple months but we had been putting off the actual process of building the stairs.

![](http://jaybigguy10.gitlab.io/blogdev/img/IMG_20200409_140214.jpg)

## Supply Chain Problems

We realized that unless we wanted to wait until the end of quarantine we would have to use some slightly different parts from local (within NZ) places as the aliexpress shipping became very iffy around the start of quarantine, I still haven't received the arduino jumpers and header strips I ordered weeks before quarantine. Products can be found on the deck build post unless linked.

We used the same IP67 5050 led strips but brought them from a local supplier for only $2 more per five meter strip, buying the aluminum profile locally on the other hand cost much more at $13.30 instead of $5.60 a meter. They will be powered off the already locally supplied power supply we installed for the back wall and we bought another one to continue running the back wall as the last free relay on the 4ch sonoff ran to the other end of the deck. We also bought more heatshrink, terminal strips and wire from the local electrical store.

We sourced much cheaper conduit and fittings though, this time from Bunnings ([Flex Conduit](https://www.bunnings.co.nz/deta-corrugated-conduit-medium-duty-20mmx20m-2620b-20_p4330838)) ([Saddle Clamps](https://www.bunnings.co.nz/deta-conduit-fittings-pvc-mounting-saddle-20mm-2pk-grey_p0235097)) ([Inspection Tees](https://www.bunnings.co.nz/deta-conduit-fittings-inspection-tee-20mm-grey_p0235125)) ([1way Junctions](https://www.bunnings.co.nz/deta-conduit-fittings-1way-lh-junction-box-20mm-grey_p0235128))

## Build Process

We routed out slots in the vertical step boards so the aluminum profile could be mounted flush with the front face of the boards and we then drilled a hole through to the other side of the board at a 45 degree angle. Slot with test profile pictured.

![](http://jaybigguy10.gitlab.io/blogdev/img/IMG_20200411_142451.jpg)A one way junction box is mounted over the back of the hole on the other side of the step which goes through the runner board.

![](http://jaybigguy10.gitlab.io/blogdev/img/received_285327872483919.jpeg)

There is a main conduit which the pass throughs join to with tee junctions on the side of the runner board which gets covered by capping

![](http://jaybigguy10.gitlab.io/blogdev/img/received_648772789302067.jpeg)

A large advantage of using flex conduit for this job can be seen here where it means we didn't have to worry about lining it perfectly up.

![](http://jaybigguy10.gitlab.io/blogdev/img/received_584774849064311.jpeg)

These angles show how hidden the conduit is behind the capping of the unstained board

![](http://jaybigguy10.gitlab.io/blogdev/img/received_2726519957620604.jpeg)

![](http://jaybigguy10.gitlab.io/blogdev/img/received_1504722616374905.jpeg)

Once all of the conduit is in place we then cut the aluminum down to length as we brought 6 profiles at 2 meter lengths, mounting them caused problems as the slot was too short for the clips to properly temporarily expand to clip once we had put the board above it over the slot so we just drilled holes in the profile and screwed them in with low profile head screws.

![](http://jaybigguy10.gitlab.io/blogdev/img/img_20200510_103301.jpg)

We cut, soldered and heatshrinked the light strips into meter long sections and put them into the profile, a useful tip for putting them into horizontal profile is to use bluetack to hold one end in. After putting them into the profile the wires in the junction boxes were joined which were a wire feeding power from the previous strip, a short wire to the strip and a wire leading to the next strip. Here is how we tried to make sure the strips stayed IP67:

![](http://jaybigguy10.gitlab.io/blogdev/img/2019/deck-20.jpg)

## Powering the whole mess

The final strip at the top of the stairs is right next to where the power supply is for  the back wall so we connected the new stairs to it and installed a new power supply for the back wall at the other end of it. This is because where the wall was hooked up only had 2 switched 240v cables and they were both being used but there was an unused 240v cable at the other end of the wall. This leads to some horrible spaghetti monsters. This is the Tasmota flashed sonoff 4ch that makes the whole thing smart, the grouping of live, neutral and ground is what leads to the mess.

![](http://jaybigguy10.gitlab.io/blogdev/img/img_20200607_183919.jpg)

Connecting the stairs to the existing supply, 12v box is open in shown picture, the power runs to the stairs runs through the new flex conduit. Also shown is the disconnected end of the wall's power cable.

![](http://jaybigguy10.gitlab.io/blogdev/img/img_20200607_174446.jpg)

The new box to power the existing back wall, 240v comes through the wall into a hole in the back of the box and 12v comes out the flexduit to run to the first joiner block of the wall.

![](http://jaybigguy10.gitlab.io/blogdev/img/img_20200607_180019.jpg)

## The Smarts

The stairs are connected to one of the channels of the existing 4ch sonoff, this talks to home assistant through an mqtt broker, they are then displayed as a button in Lovelace, code examples can be found at the bottom of the post.

![](http://jaybigguy10.gitlab.io/blogdev/img/lights.PNG)

## Completed project photos

![](http://jaybigguy10.gitlab.io/blogdev/img/img_20200607_151816.jpg)![](http://jaybigguy10.gitlab.io/blogdev/img/img_20200607_184033.jpg)![](http://jaybigguy10.gitlab.io/blogdev/img/img_20200607_183958.jpg)![](http://jaybigguy10.gitlab.io/blogdev/img/img_20200607_183936.jpg)

## Future Plans & Conclusion

We have future plans for automation of the stair lights, these are to drive a relay with an led light driver which is controlled by a motion sensor in order to give an input to an esp8266 to then turn the lights on at night, this will be clearer in another post if it is ever made.

The finishing of the non lighting aspect of the stairs was mostly done at time of finishing but the stairs probably need one more layer of staining and plants still need to go in around the sides and in the gap between the light and the concrete slabs.

This was a great project to work on over the lockdown period here in new Zealand and we got it complete not long after it ended, working around supply problems for needed components by sourcing them from local companies caused a higher cost in some areas like electronics and lower prices in areas like conduit and fittings but was certainly an interesting project.

## Extra Photos & Code

240 volt being wired to back wall

![](http://jaybigguy10.gitlab.io/blogdev/img/img_20200607_151909.jpg)

12 volt join to back wall

![](http://jaybigguy10.gitlab.io/blogdev/img/img_20200607_175952.jpg)

Looking down the completed stairs

![](http://jaybigguy10.gitlab.io/blogdev/img/img_20200607_151735.jpg)

<script src='https://gitlab.com/snippets/1985691.js'></script>
<script src='https://gitlab.com/snippets/1900101.js'></script>