---
title: "Turning the deck lights on automatically at night"
date: 2020-03-04T10:36:47+13:00
draft: false
---

In one of my previous posts I installed 10 meters of led lights on my decks retaining wall and more on the breakfast bar and stairs for outdoor lighting at night, 
now since this is controlled with home assistant I wanted to automate them to come on at night if I was home.

I have thrown together a quick node red flow to do it and it works perfectly, I like it because it helps to keep track of time at the end of the day and the other people in the house usually always notice it and go "Oh look at that the sun just set", here is my guide for the easyist to setup automation for any lights or process you want to happen at night.
  
I am using node-red to do all of these automations as it is the quickest way to prototype and try out automations for me, here is [Juan's Guide](https://www.juanmtech.com/get-started-with-node-red-and-home-assistant/) to set it up. 
 
How the automation works:<br>
When the sun goes down it will check if the house is occupied and if it is then it turns the lights on and three hours later turns the lights back off, but if your occupation changes to away it will turn them off immediately. If you happen to come home after sunset it will also turn on the lights then turn them off three hours later. 
 
In Action: (Looped Gif)<br>
![In action](../../img/2020/in-action.gif)

Setup Guide: 
To import it into node red copy the code from my gitlab snippet at the bottom of this post, open the hamburger menu in the top right of node red, select import then paste in the code. 
Change the input_boolean.occupation to your home assistant entity for tracking house occupation, change the delay node to your chosen ammount of time for the lights to stay on for, then change the svc: light:turn_off/on to tunn off/on your light and finnaly deploy.

<script src="https://gitlab.com/snippets/1947267.js"></script>