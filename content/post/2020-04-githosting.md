---
title: "How I Host My Blog and how you can host one too"
date: 2020-04-01T12:35:23+13:00
draft: false
---

When I wanted to post about my little home assistant project with the esp8266 controlling devices with infrared I needed a blogging platform, none of the platforms that I found suited my needs, either they didn't give me enough control or they required payment that I cant afford as a student.
I could have ran a docker container on my own server that I run and forwarded ports and pointed a No-IP Domain to my ip, but I wasnt intrested in the security issuses that that brings so I decided on GitLab pages instead of GitHub pages partly becuase I already have my own projects on GitLab and it wasn't blocked at my college at the time.

Here is how easy it is to host on GitLab pages for no cost at all.

First there are some requirements, one is that you have a gitlab account or have created one and two is that you have a static html site to host, this isn't a guide on how to make the site itself but just how to host it.

Firstly log into your account and navigate to view your projects and create a new project
![Hostimage](../../img/2020/host-1.png)

Select the tab titled "create from Template"
![Hostimage](../../img/2020/host-2.PNG)

Then select the project "Plain HTML"  
![Hostimage](../../img/2020/host-3.PNG)

Give your project a name, it is important if you want to access your website at username.gitlab.io instead of username.gitlab.io/repositoryname you must set the repository name to username.gitlab.io. I just called mine "norfolknetblog" when I made it which was a mistake I want to fix. Then give a short description of what your blog is about incase anyone decides to look at the source code for your blog.
![Hostimage](../../img/2020/host-4.PNG)

Make sure that the project visibility is set to "Public" so that people can access your blog and press "Create Project"

![Hostimage](../../img/2020/host-5.PNG)
Once the project for your blog is created scroll down the sidebar to "CI/CD" and open it

![Hostimage](../../img/2020/host-6.PNG)
Select run pipeline and run the pipeline without changing any of the options

![Hostimage](../../img/2020/host-7.PNG)
![Hostimage](../../img/2020/host-8.PNG)
Once the Job changes to green/complete go to the sidebar again go to settings then pages

![Hostimage](../../img/2020/host-9.PNG)
![Hostimage](../../img/2020/host-10.PNG)
On the Settings page there is a link to where you can find your blog, it might not be available from the first deployment for up to half an hour but usually it doesn't take anywhere near that long

![Hostimage](../../img/2020/host-11.png)
This is what the website will look like when you can access it.

![Hostimage](../../img/2020/host-14.PNG)
Once you can access your blog you can commit your own website code into the folder called "Public" and a pipeline should automatically run for each commit which will update your website as soon as the pipeline finishes

![Hostimage](../../img/2020/host-12.PNG)
![Hostimage](../../img/2020/host-13.PNG)