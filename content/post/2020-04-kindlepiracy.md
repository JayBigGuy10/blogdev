---
title: Sailing the high sea's with the Kindle, a guide to digital permanency & free
  e-books
date: 2020-04-01T12:55:14.000+13:00
draft: true

---
E-Books are just about the easiest form of media to distribute, they have small file sizes, are contained in mostly standardized formats and are very easy to convert to other formats. This is a hopefully complete guide on how to obtain, convert and transfer DRM free ebooks to most kindle devices.

Step One - Places to find books to download<br> I find most of my books on these websites as they are easier than torrents: <a href="https://libgen.is/">libgen.is</a> is a reliable place to find fiction, comics and scientific articles, just remember to change the selector to the kind of book you want under the search bar. <a href="https://b-ok.org/">b-ok.org</a> and <a href="https://libgen.me/">libgen.me</a> are other websites where other books can also be found. If you can't or don't want to find books from these websites try searching any general purpose torrent tracker.

Step Two - Getting the right file format<br> Kindle devices only accept two file types, one of them is the proprietary .mobi format and the other is .pdf, neither of these are the best file types

Step Three - Configuring Amazon<br> Once you have the book files in the correct format the easiest method of transferring them is to email them to the amazon servers, in order to do this use a phone or desktop browser to login to the amazon account on the kindle, navigate to the \[content missing\] section. Add