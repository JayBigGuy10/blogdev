---
title: My Experience With The Home Assistant Podcast
date: 2020-04-08

---
On the 3rd/4th of April I was on the Home Assistant Podcast, I want to share my experience of being on the podcast and the process of becoming a guest.

I was not invited specially to the podcast, I reached out to Phil and Rohan first at [feedback@hasspodcast.io](mailto:feedback@hasspodcast.io "hass podcast email mailto link"), the email they advertise at the end of every episode of the podcast. So if you want to come on to the podcast and talk about your home assistant setup don't think that you have to be relevant enough to be noticed and invited by Rohan or Phil. In the first email I sent I provided some information about myself, provided some links to my social media and asked about the podcast recording schedule.

The recording times are generally on the weekends. Phil is in Australia and Rohan Canada, so the times are generally between 12pm-2pm GMT or 11pm (ish) GMT on Fridays / Saturdays / Sundays. They try make something work where possible, before a recording a survey will be sent where everyone says when they are available, and the date where everyone matches up wins. With this in mind I confirmed that I would probably be able to make it on to the podcast and asked if I could come on.

Phil then sent back a google docs form which I wont link here, in the form it gets your Name, Country, Avatar, Socials and a short intro about yourself to put into the podcast description. He also sent back some dates for when I would be coming on

* E65 - April 04/05/06 (Putdown as Backup guest if the booked guest goes MIA)
* E66 - April 25/26/27 (My Episode)

Around 10.30am on Saturday the 4th Phil messaged me to tell me that the booked guest had gone missing in action and asked me if I could come on so I sorted my stuff out and got on the call. There is a bit of friendly chat beforehand but they save most of their questions for your section later on in the podcast. Preparing some outline responses to some of the example questions they have helps to keep the interview section of the podcast flowing.

The podcast is recorded with [squadcast.fm](squadcast.fm "squadcast link") meaning that while it is a conference call your audio is recorded locally to your machine and uploaded afterwards so it is better to have a decent microphone than worrying about your internet connection.

I had a great time on the podcast, and you should too.

Listen to my episode here:
<iframe src="https://www.listennotes.com/embedded/e/1078b4e1d1af4ca1a350b9eef5879601/" width="100%" style="width: 1px; min-width: 100%;" frameborder="0" scrolling="no"></iframe>
License granted by hasspodcast.io to embed this episode
<br><br>

![](http://jaybigguy10.gitlab.io/blogdev/img/received_3052869008068378.jpeg)

![](http://jaybigguy10.gitlab.io/blogdev/img/2019/separator.png)

My Prepared Answers / Show Notes:

Our listeners love hearing about other real-world uses of Home Assistant. The questions below are ice-breaker questions we'll ask you about your Home Assistant setup.

How long have you been using Home Assistant?

Since Around March/April 2019 or around the 0.90 release, it was very confusing to install home assistant back then, I thought I wanted to run Raspbian as all the tutorials at the time were for using the system terminal but I couldn't get that to work, then when I tried to run hassio as it was known I tried to use a mouse and keyboard during setup which was also a mistake, then I was finally able to install it when I realised I was supposed to flash home assistant and let it be.

I'm still running on my original SD card and I've only had to reflash it once and it was human error, in 0.97 when python 3.5 was dropped I didn’t update HACS beforehand meaning it was still trying to run python 3.5 in 0.97 and home assistant just stopped working properly, that’s when I implemented the automatic google drive backups

How long have you been into Home Automation?

Been tech enthusiast for a long time, only got into it when my dad brough himself a 3rd gen echo dot the Christmas 2018/19, he even wrapped it up and put it under the tree to himself. But I was aware of amazon echos/ google home products beforehand(pre wiring new house, russound/old house wiring)

How do you primarily drive your Home Assistant? Voice, tablets, UI etc?

Primarily with automations and voice

Did you use any other home automation controllers before Home Assistant?

No None at all actually

How are you running Home Assistant? Raspberry Pi, [HASS.IO](https://www.google.com/url?q=http://HASS.IO&sa=D&ust=1582867315403000&usg=AFQjCNGhZzU9w2jMWcpVARhM01Oe39Pjgg), Docker etc.

On a raspberry pi 3 model b, I do run a nuc media/vpn/adblock server but it runs windows and I don’t want to do home assistant on that and a synology nas which Is probably to underpowered to run home assistant smoothly enough

What components are you using in your smart home? Hue, LifX, Z-wave, etc.

I only have wifi devices in my smart home so far, I have a 4ch sonoff controlling my deck light project, 2echo dot 3rd gens, 2echo dot 2nd gens and a wemos d1 mini board driving my infrared control. As I said in my email I am operating on a pretty tight budget and I didn’t personally buy most of these devices myself but I am looking about buying a cc25 series zigbee controller and some of the new sonoff zbr1's to automate some lights

Are you using anything for presence detection (home/away status), if so what?

Pretty much just using the nmap sensor, I would like to use one that integrates better with the router but I just have an isp standard router so that isn't possible at this point. I don’t have the desire/need to be sucking battery from devices all day for gps tracking and also doesn’t require any app at all.

I do have input Booleans setup for occupation and guest mode and they can be toggled from my amazon alexas

How many people in your smart home?

There is usually one to three people, it is interesting managing a smart home/home lab when you aren't there 40% of the time

What are some of your favourite automations/routines you've setup with Home Assistant, and how they make your life better.

My favourite automation I have set up is a simpler one, I have my 10m strip of leds turn at sunset if occupation is on and turn them off 3 hours later, I like it because it helps to keep track of time at the end of the day and the other people in the house usually always notice it and go "Oh look at that the sun just set".

My more useful ones are turning on the dumb amplifier connected to our main amazon echo and changing it to the right input when occupation comes on, turning the cinema amp on when the tv turns on and other simple automations like that.

I am driving the ones I am most reliant upon from node_red but I have been trying to get the yaml automations to work for me.

Tales of woe:

My installation of my deck light strips was a bit of a inadvertent nightmare, the powersupplies were a pain to wire up,we accidently put 2 screws through a wall voltage cable that luckily wasn't live and worst of all I killed my nas! We were turning off breakers trying to figure out which one had the outdoor plugs we were tapping on it and we flipped the one with our wd nas and it never turned back on, took 8 weeks until I could get a new synology nas.

If you have any problems or something you're trying to do in Home Assistant, and would like to ask for our opinion (or shoutout to listeners to reach out to you), also feel free to ask us. Our listeners enjoy hearing problem solving on the Podcast.

Garage door / alarm cable

Home assistant core update button improvements