---
title: Why Git should be used in programming courses
date: 2020-04-28T12:00:00.000Z
draft: true
---
Git is a powerful tool not only for version control but is also the core of nearly every systems deployment pipeline