---
title: About The Author
date: 2004-08-03T20:49:29.000+00:00
type: about

---
Git - [Hub](https://github.com/JayBigGuy10) / [Lab](https://gitlab.com/JayBigGuy10)

Twitter - [@JayBigGuy10](https://twitter.com/jaybigguy10)

Reddit - [u/JayBigGuy10](https://www.reddit.com/user/jaybigguy10)

Instagram - [@JayBigGuy10](https://www.instagram.com/jaybigguy10/?hl=en)

I am Jayden Litolff Aka JayBigGuy, I am a third year college student in New Zealand

I love Reddit, YouTube, Minecraft, Titanfall, Home Assistant, The Expanse, Raspberry Pi's, Homelabs, Data hoarding and Plex and I have experience with Python, html/CSS, Docker, Server Administration, Soldering, Micro controllers and Torrenting / Media Management

Other activities that I enjoy that may come up on this blog are reading lots of books, I love a great Sci-Fi Novel especially space related, I'm really into anything NASA, SpaceX and Apollo Program Etc. I also love to get out on my mountain bike and go for a ride but that doesn't happen often enough with college work.

Also Cats,
I have two fluffy cats, this is Jasper:

![Example image](/img/cat.jpg)